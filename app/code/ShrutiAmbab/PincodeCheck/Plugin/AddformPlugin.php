<?php
namespace ShrutiAmbab\PincodeCheck\Plugin;

class AddformPlugin{
    public function beforeSetTitle(\ShrutiAmbab\PincodeCheck\Controller\Adminhtml\Index\NewIndividual $subject, $title){
        $title='Welcome To '.$title; //changed arg of setTitle method
        //echo __METHOD__ . "</br>";
        return $title;
    }
    public function afterGetTitle(\ShrutiAmbab\PincodeCheck\Controller\Adminhtml\Index\NewIndividual $subject, $result){
        //echo __METHOD__ . "</br>";
        return $result.' Form'; //changed return value of getTitle
    }
    public function aroundGetTitle(\ShrutiAmbab\PincodeCheck\Controller\Adminhtml\Index\NewIndividual $subject, callable $proceed){
        //echo __METHOD__ . " - Before proceed() </br>";
        $result = $proceed(); //control will go to observed method as there in no one else next in chain
        //echo __METHOD__ . " - After proceed() </br>";
        return $result;
    }
}