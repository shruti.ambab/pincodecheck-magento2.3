<?php

namespace ShrutiAmbab\PincodeCheck\Setup;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;


class InstallSchema implements InstallSchemaInterface
{
   
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
          /**
          * Create table 'contactData'
          */
          $table = $setup->getConnection()
              ->newTable($setup->getTable('pincodeData'))
              ->addColumn(
                  'id',
                  Table::TYPE_INTEGER,
                  null,
                  ['nullable' => false,
                  'identity'=>true,
                  'primary'=>true,
                  'unsigned' => true],
                  'Index Field'
                )
              ->addColumn(
                'pincode',
                Table::TYPE_NUMERIC,
                255,
                ['nullable' => false,
                 'unsigned' => true,
                 'precision' => 6],
                'Pincode'
                )
              ->addColumn(
                  'serviceability',
                  Table::TYPE_BOOLEAN,
                  null,
                  ['nullable' => false,
                    'default' => 1],
                  'Serviceability'
              )
              ->addColumn(
                'created_at',
                Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false,
                 'default' => Table::TIMESTAMP_INIT],
                  'Created_At'
              )
              ->addColumn(
                'updated_at',
                Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false,
                'default' => Table::TIMESTAMP_INIT_UPDATE],
                'Updated_At'
              )->setComment('Pincode Table');
          $setup->getConnection()->createTable($table);

          $setup->getConnection()->addIndex(
            $setup->getTable('pincodeData'),
            $setup->getIdxName(
            $setup->getTable('pincodeData'),
            ['pincode'],
            AdapterInterface::INDEX_TYPE_UNIQUE
            ),
            ['pincode'],
            AdapterInterface::INDEX_TYPE_UNIQUE
        );
      }
}
