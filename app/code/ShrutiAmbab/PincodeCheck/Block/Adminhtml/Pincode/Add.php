<?php

namespace ShrutiAmbab\PincodeCheck\Block\Adminhtml\Pincode;

class Add extends \Magento\Backend\Block\Widget\Container
{

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    protected function _prepareLayout()
    {
        $addButtonProps = [
            'id' => 'add_new',
            'label' => __('Add New Pincode'),
            'class' => 'add',
            'button_class' => '',
            'class_name' => 'Magento\Backend\Block\Widget\Button\SplitButton',
            'options' => $this->_getAddButtonOptions(),
        ];
        $this->buttonList->add('add_new', $addButtonProps);

        return parent::_prepareLayout();
    }

    /**
     * Retrieve options for 'CustomActionList' split button
     *
     * @return array
     */
    protected function _getAddButtonOptions()
    {
        /*list of button which you want to add*/
        $splitButtonOptions=[
        'action_1'=>['label'=>__('Individually'),'onclick'=>"setLocation('" . $this->_getCreateUrl() . "')"],
        'action_2'=>['label'=>__('In Bulk'),'onclick'=>"setLocation('" . $this->_getAddUrl() . "')"]
        ];
        /* in above list you can also pass others attribute of buttons*/
        return $splitButtonOptions;
    }
    protected function _getCreateUrl()
    {
        return $this->getUrl(
            'pincodechecka/index/newIndividual'
        );
    }
    protected function _getAddUrl()
    {
        return $this->getUrl(
            'pincodechecka/index/newBulk'
        );
    }
}
