<?php
namespace ShrutiAmbab\PincodeCheck\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
class Sayhi extends Command
{
    protected function configure()
    {
        $this->SetName('message:hi');
        $this->setDescription('Demo command line');

        parent::configure();
    }
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Hii");
    }
}