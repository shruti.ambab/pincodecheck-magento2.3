<?php
/**
* Copyright © Magento, Inc. All rights reserved.
* See COPYING.txt for license details.
*/
namespace ShrutiAmbab\PincodeCheck\Api; 
/**
* @api
*/
interface PageInterface{
      /**
   * 
   * @param string[] $title
   * @return anyType
   */
    public function importpage($title);
}