<?php
namespace ShrutiAmbab\PincodeCheck\Controller\Adminhtml\Index;

use ShrutiAmbab\PincodeCheck\Model\ResourceModel\Pincode\CollectionFactory;

class SaveButtonAddIndividual extends \Magento\Backend\App\Action
{

    //const ADMIN_RESOURCE = 'Index';

    protected $resultPageFactory;
    protected $pincodeFactory;
    protected $_collection;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \ShrutiAmbab\PincodeCheck\Model\PincodeFactory $pincodeFactory,
         CollectionFactory $collection
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->pincodeFactory = $pincodeFactory;
        $this->_collection=$collection;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getParam('pincode');
        //var_dump($data);exit;
        $resultRedirect = $this->resultRedirectFactory->create();
        

        if($data)
        {
            try{
                if(empty($data['pincode'])){
                    throw new \Magento\Framework\Exception\LocalizedException(__('Enter pincode first.'));
                }
                else{
                    $pincode = $data['pincode'];
                    $pincodecollection= $this->_collection->create();
                    $check = $pincodecollection->addFieldToFilter('pincode',['eq'=>$pincode]);//check for duplicates
                    if(count($check->getData())){
                        throw new \Magento\Framework\Exception\LocalizedException(__('Pincode already exists.'));
                    }
                    else if(!is_numeric($pincode)){
                        throw new \Magento\Framework\Exception\LocalizedException(__('Pincode should be numeric.'));
                    }
                    else if(filter_var($pincode, FILTER_VALIDATE_INT) === false ){ 
                        throw new \Magento\Framework\Exception\LocalizedException(__('Pincode should be an integer.'));
                    }
                    else if(strlen($pincode)!=6){
                        throw new \Magento\Framework\Exception\LocalizedException(__('Pincode can be 6 digits only.'));
                    } 
                    else if(preg_match('/\s/',$pincode)){ //white spaces //no need numeric doesnt accept spaces
                        throw new \Magento\Framework\Exception\LocalizedException(__('No spaces allowed!'));
                    }
                    else if($pincode{0}==0){ //charAt type | 1st digit of pincode 
                        throw new \Magento\Framework\Exception\LocalizedException(__('Pincode can not start with zero.'));
                    }
                    else{
                        $pincoderecord = $this->pincodeFactory->create()->load($pincode);
                        //var_dump($data); exit;
                        $pincoderecord->setData($data);
                        $pincoderecord->save();
                        $this->messageManager->addSuccess(__('Successfully saved the pincode.'));
                        $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                        return $resultRedirect->setPath('*/*/');
                    }
                }
            }
            catch(\Exception $e)
            {
                $this->messageManager->addError($e->getMessage());
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData($data);
                return $resultRedirect->setPath('*/*/newIndividual');
            }
        }

         return $resultRedirect->setPath('*/*/');
    }
}