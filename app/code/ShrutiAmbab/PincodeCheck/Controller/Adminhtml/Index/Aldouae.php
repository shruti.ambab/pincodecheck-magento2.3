<?php
namespace ShrutiAmbab\PincodeCheck\Controller\Adminhtml\Index;

use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action\Context;

use Magento\Framework\Event\Manager;

Class Aldouae extends \Magento\Backend\App\Action //remember to write the class name same as file name
{

    protected $pageFactory;
    private $eventManager;

    public function __construct(Context $context, PageFactory $pageFactory,Manager $eventManager)
    {
        $this->pageFactory = $pageFactory;
        $this->eventManager = $eventManager;
        return parent::__construct($context);
    }

    public function execute()
    {          
        $page_object = $this->pageFactory->create();

        $page_object->getConfig()->getTitle()->prepend('Aldo UAE Settings');
        return $page_object;
    }    
}
