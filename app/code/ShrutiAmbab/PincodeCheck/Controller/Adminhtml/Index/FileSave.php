<?php
namespace ShrutiAmbab\PincodeCheck\Controller\Adminhtml\Index;

use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action\Context;
use ShrutiAmbab\PincodeCheck\Model\PincodeFactory;
use Magento\Framework\File\Csv;
use ShrutiAmbab\PincodeCheck\Model\ResourceModel\Pincode\CollectionFactory;


Class FileSave extends \Magento\Backend\App\Action //remember to write the class name same as file name
{


    protected $pageFactory;
    protected $_collection;
    protected $_pincodefactory;


    public function __construct(Context $context, PageFactory $pageFactory,Csv $csvProcessor,CollectionFactory $collection,PincodeFactory $pincodefactory)
    {
        // echo 'hiii';exit;
        $this->csvProcessor = $csvProcessor;
        $this->pageFactory = $pageFactory;
        $this->_pincodefactory=$pincodefactory;
        $this->_collection=$collection;
        parent::__construct($context);   
    }

    public function execute()
    {          
        //var_dump("filesave controller");exit;
        // $page_object = $this->pageFactory->create();
        // $page_object->getConfig()->getTitle()->prepend(__('Bulk Controller'));
        // return $page_object;
        $resultRedirect = $this->resultRedirectFactory->create();
        $file=$this->getRequest()->getFiles();
        //var_dump($file);exit;
        
        //echo "<pre>";print_r($importrawdata);exit;
        //var_dump($importrawdata);exit;
        //var_dump(count($importrawdata[0]));exit;
        
        //validations
        //echo "<pre>";print_r($importrawdata);exit;
        //validate csv
        if (!isset($file['file']['tmp_name'])) {
            $this->messageManager->addError(__('Invalid file.'));
            return $resultRedirect->setPath('*/*/newBulk');
        }
        else if(empty($file['file']['tmp_name'])) {
            $this->messageManager->addError(__('No file chosen.'));
            return $resultRedirect->setPath('*/*/newBulk');
        }
        else if($file['file']['size']==0){
            $this->messageManager->addError(__('File can not be empty.'));
            return $resultRedirect->setPath('*/*/newBulk');
        }
        else if($file['file']['type']!='text/csv'){
            $this->messageManager->addError(__('Invalid file. File must be a csv file.'));
            return $resultRedirect->setPath('*/*/newBulk');
        }

        $importrawdata=$this->csvProcessor->getData($file['file']['tmp_name']);
        //echo "<pre>";print_r($importrawdata);exit;
        //var_dump($importrawdata);exit;
        //validate csv file format
        $error=0;
        //echo "<pre>";print_r($importrawdata);exit;
        foreach ($importrawdata as $rowIndex => $dataRow) {
            if(count($importrawdata[$rowIndex])!=2){
                $error++;
                break;
            }
        }
        if($error!=0)
        {
            $this->messageManager->addError(__('Invalid csv file format. Please refer sample file.'));
            return $resultRedirect->setPath('*/*/newBulk');
        }
        else if(($importrawdata[0][0]!='pincode') || ($importrawdata[0][1]!='serviceability')){
            $this->messageManager->addError(__('Invalid csv file format. Please refer sample file.'));
            return $resultRedirect->setPath('*/*/newBulk');
        }
        //validate csv file data
        $error=0;
        // echo gettype($importrawdata[1][0]);
        $i=0;
        //echo count($importrawdata);exit;
        //echo $error; exit;
        for($i=1;$i<count($importrawdata);$i++){
            //$importrawdata[i][0]
            //pincode validation
            if(empty($importrawdata[$i][0])){
                $error++;
                break;
            }
            if(!is_numeric($importrawdata[$i][0])){
                //echo("isnumeric");exit;
                $error++;
                break;
            }
            else if(strlen($importrawdata[$i][0])!=6){
                //echo("strlen");exit;
                $error++;
                break;
            } 
            else  if(preg_match('/\s/',$importrawdata[$i][0])){ //white spaces //no need numeric doesnt accept spaces
                //echo("spaces");exit;
                $error++;
                break;
            }
            else if($importrawdata[$i][0]{0}==0){ //charAt type | 1st digit of pincode 
                //echo("firstchar");exit;//cant be 0
                $error++;
                break;
            }
            //serviceability validation
            else if( ($importrawdata[$i][1]!=0) && ($importrawdata[$i][1]!=1) ){
                //echo("0 or 1");exit;
                $error++;
                break;
            }
            else if( filter_var($importrawdata[$i][1], FILTER_VALIDATE_INT) === false ){
                //echo($importrawdata[$i][1]);exit;
                //echo("is interger");exit;
                $error++;
                break;
            }
        }
        //exit;
        //echo $error; exit;
        if($error!=0){
            $this->messageManager->addError(__('Invalid csv file data. Please refer sample file.'));
            return $resultRedirect->setPath('*/*/newBulk');
        }
        else{
            //echo "<pre>";print_r($importrawdata);
            $duplicate=0;
            $nonduplicate=0;
            $model = $this->_pincodefactory->create();
            // $pincode= $this->_collection->create();
            foreach ($importrawdata as $rowIndex => $dataRow) {
                // echo "<pre>";print_r($rowIndex);print_r($dataRow);continue;
                $pincode= $this->_collection->create();
                if($rowIndex>0){
                    //var_dump($dataRow[1]);exit;
                    $pin=(int)$dataRow[0];
                    $sts=(int)$dataRow[1];
                    //var_dump($dataRow);
                    // var_dump($pin);
                    // var_dump($sts);
                    // exit;
                    $check = $pincode->addFieldToFilter('pincode',['eq'=>$pin]);//check for duplicate //it returns entry in database that satisfies the addFieldToFilter condition
                    //echo "<pre>";print_r($check->getData());exit;
                    if(count($check->getData())){
                        $duplicate++;
                    }
                    else{
                        $data=['pincode'=> $pin,'serviceability'=> $sts];
                        //print_r($data);
                        $nonduplicate++;
                        $model->setData($data);
                        $model->save();
                    }
                }
            }  
            // echo $duplicate;
            // echo $nonduplicate;exit;
            $this->messageManager->addSuccess(__($nonduplicate.' records inserted and '.$duplicate.' duplicate entries rejected.'));
            return $resultRedirect->setPath('*/*/newBulk');  
        }    
    }    
}