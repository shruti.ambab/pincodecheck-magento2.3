<?php
namespace ShrutiAmbab\PincodeCheck\Controller\Adminhtml\Index;

use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action\Context;



Class NewBulk extends \Magento\Backend\App\Action //remember to write the class name same as file name
{
    protected $pageFactory;
    protected $resultJsonFactory;

    public function __construct(Context $context, PageFactory $pageFactory)
    {
        $this->pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {          
        $page_object = $this->pageFactory->create();

        $data=["title"=>["name"=>"shruti","beforetitle"=>"Page to"]
        ];
        $data=json_encode($data);
        //var_dump($data);exit;
        $ch = curl_init("http://pincodecheck.com/rest/V1/import/name");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $result = curl_exec($ch);
        curl_close($ch);
        $page_title=json_decode($result,true);
        //$result=json_decode($result,true);
        //var_dump($page_title);exit;
        $page_object->getConfig()->getTitle()->prepend($page_title);
        return $page_object;
    }    
}