<?php
namespace ShrutiAmbab\PincodeCheck\Controller\Adminhtml\Index;

use ShrutiAmbab\PincodeCheck\Model\ResourceModel\Pincode\CollectionFactory;

class SaveButton extends \Magento\Backend\App\Action
{

    //const ADMIN_RESOURCE = 'Index';

    protected $resultPageFactory;
    protected $pincodeFactory;
    protected $_collection;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \ShrutiAmbab\PincodeCheck\Model\PincodeFactory $pincodeFactory,
        CollectionFactory $collection
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->pincodeFactory = $pincodeFactory;
        $this->_collection=$collection;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getParam('pincode');
        //var_dump($data);exit;
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $data['id'];
        $pincode = $this->pincodeFactory->create()->load($id);

        if($data)
        {
            try{
                $data = array_filter($data, function($value) {return $value !== ''; });
                //var_dump ($data); exit;
                //var_dump ($data["pincode"]); exit;  //string
                //var_dump(isset($data["pincode"]));exit;
                if(!isset($data["pincode"])){
                    throw new \Magento\Framework\Exception\LocalizedException(__('Enter pincode first.'));
                }
                else{
                    $pin=$data["pincode"];
                    //var_dump($pin);exit;
                    //var_dump(is_numeric($pin));exit;
                    $pincodecollection= $this->_collection->create();
                    $check = $pincodecollection->addFieldToFilter('pincode',['eq'=>$pin]);//check for duplicates
                    if(count($check->getData())){
                        throw new \Magento\Framework\Exception\LocalizedException(__('Pincode already exists.'));
                    }
                    else if(!is_numeric($pin)){
                        throw new \Magento\Framework\Exception\LocalizedException(__('Pincode should be numeric.'));
                    }
                    else if(filter_var($pin, FILTER_VALIDATE_INT) === false ){ 
                        throw new \Magento\Framework\Exception\LocalizedException(__('Pincode should be an integer.'));
                    }
                    else if(strlen($pin)!=6){
                        throw new \Magento\Framework\Exception\LocalizedException(__('Pincode can be 6 digits only.'));
                    } 
                    else if(preg_match('/\s/',$pin)){ //white spaces //no need numeric doesnt accept spaces
                        throw new \Magento\Framework\Exception\LocalizedException(__('No spaces allowed!'));
                    }
                    else if($pin{0}==0){ //charAt type | 1st digit of pincode 
                        throw new \Magento\Framework\Exception\LocalizedException(__('Pincode can not start with zero.'));
                    }
                    else{
                    $pincode->setData($data);
                    $pincode->save();
                    $this->messageManager->addSuccess(__('Successfully saved the changes.'));
                    $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                    return $resultRedirect->setPath('*/*/');
                    }
                }
            }
            catch(\Exception $e)
            {
                $this->messageManager->addError($e->getMessage());
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData($data);
                return $resultRedirect->setPath('*/*/edit', ['id' => $pincode->getId()]);
            }
        }

         return $resultRedirect->setPath('*/*/');
    }
}