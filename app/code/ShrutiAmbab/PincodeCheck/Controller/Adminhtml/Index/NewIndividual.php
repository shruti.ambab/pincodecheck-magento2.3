<?php
namespace ShrutiAmbab\PincodeCheck\Controller\Adminhtml\Index;

use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action\Context;


Class NewIndividual extends \Magento\Backend\App\Action //remember to write the class name same as file name
{


    protected $pageFactory;
    protected $title;

    public function __construct(Context $context, PageFactory $pageFactory)
    {
        $this->pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {      
        $page_object = $this->pageFactory->create();
        $this->setTitle('Add Pincode');
        //echo $title;
        //exit;
        $page_title=$this->getTitle();

        // echo $this->setTitle('Add Pincode');
        // echo "</br>";
		// echo $this->getTitle();
        // exit;
        $page_object->getConfig()->getTitle()->prepend($page_title);
        return $page_object;
    }   
    public function setTitle($title)
	{
		return $this->title = $title;
	}
	public function getTitle()
	{
		return $this->title;
	} 
}