<?php
namespace ShrutiAmbab\PincodeCheck\Controller\Adminhtml\Index;

use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action\Context;


Class Edit extends \Magento\Backend\App\Action //remember to write the class name same as file name
{
    // const ADMIN_RESOURCE = 'Index';

    protected $pageFactory;


    public function __construct(Context $context, PageFactory $pageFactory)
    {
        $this->pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {          
        $id = $this->getRequest()->getParam('id');

        $page_object = $this->pageFactory->create();
       
        $ch = curl_init("http://pincodecheck.com/rest/V1/hello/name/$id");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
        $result = curl_exec($ch);
        $page_title=json_decode($result,true);
        //var_dump($result);exit;
        $page_object->getConfig()->getTitle()->prepend($page_title);
        return $page_object;
    }    
}