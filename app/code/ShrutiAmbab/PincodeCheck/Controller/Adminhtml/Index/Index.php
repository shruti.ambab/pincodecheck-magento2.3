<?php
namespace ShrutiAmbab\PincodeCheck\Controller\Adminhtml\Index;

use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action\Context;

use Magento\Framework\Event\Manager;

Class Index extends \Magento\Backend\App\Action //remember to write the class name same as file name
{

    protected $pageFactory;
    private $eventManager;

    public function __construct(Context $context, PageFactory $pageFactory,Manager $eventManager)
    {
        $this->pageFactory = $pageFactory;
        $this->eventManager = $eventManager;
        return parent::__construct($context);
    }

    public function execute()
    {          
        $page_object = $this->pageFactory->create();

        $page_title = new \Magento\Framework\DataObject(array('title' => 'Pincode'));
        $this->eventManager->dispatch('pincodes_listing_title', ['titledata' => $page_title]);

        $page_object->getConfig()->getTitle()->prepend($page_title->getTitle());
        return $page_object;
    }    
}
