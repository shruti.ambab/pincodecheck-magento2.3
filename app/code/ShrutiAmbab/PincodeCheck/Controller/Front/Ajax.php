<?php
namespace ShrutiAmbab\PincodeCheck\Controller\Front;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use ShrutiAmbab\PincodeCheck\Model\PincodeFactory;
use Magento\Framework\Controller\Result\JsonFactory;

class Ajax extends Action
{
    protected $pincodeFactory;
    protected $resultJsonFactory;

    public function __construct(Context $context,PincodeFactory $pincodeFactory,JsonFactory $resultJsonFactory)
    {
        parent::__construct($context);
        $this->pincodeFactory = $pincodeFactory;
        $this->resultJsonFactory = $resultJsonFactory;
    }

    public function execute()
    {   
        $pincode= $this->getRequest()->getPost('pincode_is'); 
        // echo "shruti";
        // echo $pincode; die();
        
        $status;
        $message;
        $response;
        $resultJson = $this->resultJsonFactory->create();
        $pincodedata = $this->pincodeFactory->create();
        $pincodedata->load(($pincode),'pincode');
        if($pincodedata->getid()){
            if($pincodedata->getData()['serviceability'] == 1 )
            {
                $status="success";
                $message="Delivery is Available !";
            }
            else
            {
                $status="error";
                $message="Sorry, Delivery is Not Available.";
            }
        }
        else
        {
            $status="error";
            $message="Sorry, Delivery is Not Available.";
        } 
        //var_dump($status); exit;
        $response = ["status" => $status, "message" => $message];
        $resultJson->setData($response);
        //var_dump($resultJson);exit;
        return $resultJson;
        // $pincodedata = $this->pincodeFactory->create();
        // $pincodedata->load(($pincode),'pincode');
        // if($pincodedata->getid()){
        //     if($pincodedata->getData()['serviceability'] == 1 )
        //     {
        //         echo "yes";
        //     }
        //     else
        //     {
        //         echo "no";
        //     }
        // }
        // else
        // {
        //     echo "no";
        // } 
    }
}
?>