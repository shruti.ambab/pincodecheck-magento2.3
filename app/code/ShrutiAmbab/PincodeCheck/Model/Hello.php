<?php
namespace ShrutiAmbab\PincodeCheck\Model;
use ShrutiAmbab\PincodeCheck\Api\HelloInterface;
class Hello implements HelloInterface
{
    public function name($id){
        return 'Pincode (ID-'.$id.')';
    }
}