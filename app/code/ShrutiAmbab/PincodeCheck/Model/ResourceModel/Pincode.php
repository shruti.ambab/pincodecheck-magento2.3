<?php

namespace ShrutiAmbab\PincodeCheck\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Pincode extends AbstractDb
{
    public function __construct(\Magento\Framework\Model\ResourceModel\Db\Context $context)
	{
		parent::__construct($context);
	}
    protected function _construct()
    {
        $this->_init('pincodeData', 'id');
    }
}