<?php
namespace ShrutiAmbab\PincodeCheck\Model;
use ShrutiAmbab\PincodeCheck\Api\PageInterface;
class Page implements PageInterface{

    protected $request;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request
    )
    {
        $this->request = $request;
    }
    public function importpage($title){
        //return "hi";
        //$post = json_encode($this->request->getPost());

        // $title=$title.' Import Pincodes';
        $pagetitle = $title["beforetitle"].' Import Pincodes';
        return $pagetitle;
    }
}