<?php
namespace ShrutiAmbab\PincodeCheck\Observer\Pincodes;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
class Title implements ObserverInterface{
    public function __construct()
    {
      // Observer initialization code...
      // You can use dependency injection to get any class this observer may need.
    }
    public function execute(Observer $observer){
        $eventdata = $observer->getData('titledata');
        // $page_title="Pincodes";
        $eventdata->setTitle("Pincodes");
    }
}