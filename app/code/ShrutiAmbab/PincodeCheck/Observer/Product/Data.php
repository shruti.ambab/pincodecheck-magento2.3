<?php
namespace ShrutiAmbab\PincodeCheck\Observer\Product;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
class Data implements ObserverInterface{
    public function __construct()
    {
      // Observer initialization code...
      // You can use dependency injection to get any class this observer may need.
    }
    public function execute(Observer $observer){
        $product = $observer->getProduct();
        $originalname= $product->getName();
        $modifiedname=$originalname.' Changed with observer on event catalog_controller_product_view';
        $product->setName($modifiedname);
    }
}