<?php 
namespace ShrutiAmbab\HelloPage\Controller\Index;

//manual data entry
// use Magento\Framework\Controller\ResultFactory;
// use Magento\Framework\App\Action\Action;
// use Magento\Framework\App\Action\Context;
// use ShrutiAmbab\HelloPage\Model\FormItemFactory;

// class Index extends Action{
//     protected $_formItem;
//     protected $resultRedirect;
//     public function __construct(Context $context,FormItemFactory $formItem,ResultFactory $result)
//     {
//         parent::__construct($context);
//         $this->_formItem = $formItem;
//         $this->resultRedirect = $result;
//     }
//     public function execute()
//     {
//         $resultRedirect = $this->resultRedirect->create(ResultFactory::TYPE_REDIRECT);
//         $resultRedirect->setUrl($this->_redirect->getRefererUrl());
// 		$model = $this->_formItem->create();
// 		$model->addData([
// 			"name" => 'sneha',
// 			"email" => 'sneha95@gmail.com',
//             "contact" => '95632412510',
//             "sort_order" => 1
// 			]);
//         $saveData = $model->save();
//         if($saveData){
//             $this->messageManager->addSuccess( __('Insert Record Successfully !') );
//         }
// 		return $resultRedirect;
// 	}
// }

//form data entry

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use ShrutiAmbab\HelloPage\Model\FormItemFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Session\SessionManagerInterface;

class Index extends Action
{
    protected $_modelFormItemFactory;
    protected $resultPageFactory;
    protected $_sessionManager;

    public function __construct(
        Context $context,
        FormItemFactory $modelFormItemFactory,
        PageFactory  $resultPageFactory,
        SessionManagerInterface $sessionManager
    )
    {
        parent::__construct($context);
        $this->_modelFormItemFactory = $modelFormItemFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->_sessionManager = $sessionManager;
    }

    public function execute()
    {   
        $resultRedirect     = $this->resultRedirectFactory->create();
        $FormItemModel      = $this->_modelFormItemFactory->create();
        $data               = $this->getRequest()->getPost(); 

        $FormItemModel->setData('name', $data['name']);
        $FormItemModel->setData('email', $data['email']);
        $FormItemModel->setData('contact', $data['contact']);

        $FormItemModel->save();

        $this->_redirect('test/page/form');
        $this->messageManager->addSuccess(__('The data has been saved.'));
    }
}
?>