<?php
namespace ShrutiAmbab\HelloPage\Controller\Page;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
//use Magento\Framework\Message\ManagerInterface;
 //to show message that you got the data

class Form extends \Magento\Framework\App\Action\Action
{

    protected $pageFactory;
    //protected $messageManager;
    public function __construct(Context $context, PageFactory $pageFactory)
    {
        $this->pageFactory = $pageFactory;
        //$this->messageManager = $messageManager;
        return parent::__construct($context);
    }

    public function execute()
    {          
        $page_object = $this->pageFactory->create();
        return $page_object;
    }    
}
