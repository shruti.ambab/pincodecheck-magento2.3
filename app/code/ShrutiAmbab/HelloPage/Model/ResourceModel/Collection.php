<?php 
namespace ShrutiAmbab\HelloPage\Model\ResourceModel\FormItem;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection{
    /**
     * Define model & resource model
     */
	public function _construct(){
		$this->_init("ShrutiAmbab\HelloPage\Model\FormItem","Bss\Schema\Model\ResourceModel\FormItem");
	}
}
?>